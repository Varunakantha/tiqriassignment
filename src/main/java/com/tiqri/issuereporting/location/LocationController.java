package com.tiqri.issuereporting.location;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LocationController {
	
	@Autowired
	private LocationService locationServie;

	@RequestMapping("/locations")
	public List<Location> getAllLocations () {
		return locationServie.getAllLocations();
	}
	
	@RequestMapping("/locations/{id}")
	public Location getLocation(@PathVariable String id){
		return locationServie.getLocation(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/locations")
	public void addLocation (@RequestBody Location location){
		locationServie.addLocation(location);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/location/{id}")
	public void updateLocation (@RequestBody Location location, @PathVariable String id){
		locationServie.updateLocation(location, id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/location/{id}")
	public void deleteLocation (@PathVariable String id){
		locationServie.deleteLocation(id);
	}

}
