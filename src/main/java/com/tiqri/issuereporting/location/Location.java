package com.tiqri.issuereporting.location;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Location {
	
	@Id
	private String id;
	private String assignedPerson;
	
	
	public Location(String id, String assignedPerson) {
		super();
		this.id = id;
		this.assignedPerson = assignedPerson;
	}
	
	public Location() {
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getassignedPerson() {
		return assignedPerson;
	}
	public void setassignedPerson(String assignedPerson) {
		this.assignedPerson = assignedPerson;
	}
	
	
	

}
