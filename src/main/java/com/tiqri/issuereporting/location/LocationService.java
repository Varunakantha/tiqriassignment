package com.tiqri.issuereporting.location;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationService {

	@Autowired
	private LocationRepository locationRepository ;
	
	public List<Location> getAllLocations () {
		List<Location> locations = new ArrayList<Location>();
		locations =(List<Location>) locationRepository.findAll();
	
		//Just for testing
/*		Location location0 = new Location("ground-floor","varuna");
		Location location1 = new Location("first-floor","chanuka");
		Location location2 = new Location("second-floor","chamara");
		Location location3 = new Location("third-floor","malintha");
		Location location4 = new Location("fourth-floor","sampath");
		locations.add(location0);
		locations.add(location1);
		locations.add(location2);
		locations.add(location3);
		locations.add(location4);
		*/
		return locations;
	}
	
	public Location getLocation (String id){
	return locationRepository.findOne(id);
	}

	public void addLocation(Location location) {
		locationRepository.save(location);		
	}

	public void updateLocation(Location location, String id) {
		locationRepository.save(location);		
	}

	public void deleteLocation(String id) {
		locationRepository.delete(id);
	}
	
}
