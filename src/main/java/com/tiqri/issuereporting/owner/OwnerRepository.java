package com.tiqri.issuereporting.owner;

import org.springframework.data.repository.CrudRepository;

public interface OwnerRepository extends CrudRepository<Owner,String> {

}
