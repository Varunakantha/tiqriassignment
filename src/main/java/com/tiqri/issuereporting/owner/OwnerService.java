package com.tiqri.issuereporting.owner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OwnerService {

	@Autowired
	private OwnerRepository ownerRepository ;
	
	public List<Owner> getAllOwners () {
		List<Owner> owners = new ArrayList<Owner>();
		owners =(List<Owner>) ownerRepository.findAll();
		
		//Just for testing
/*		Owner owner0 = new Owner ("varuna","Varunakantha","ground-floor");
		Owner owner1 = new Owner ("chanuka","Chanuka Ashan","first-floor");
		Owner owner2 = new Owner ("chamara","Chamara Nuwan","second-floor");
		Owner owner3 = new Owner ("malintha","Malintha Kumara","third-floor");
		Owner owner4 = new Owner ("sampath","Sampath Bandara","fourth-floor");
		owners.add(owner0);
		owners.add(owner1);
		owners.add(owner2);
		owners.add(owner3);
		owners.add(owner4);*/
		
		return owners;
	}
	
	public Owner getOwner (String id){
	return ownerRepository.findOne(id);
	}

	public void addOwner(Owner owner) {
		ownerRepository.save(owner);		
	}

	public void updateOwner(Owner owner, String id) {
		ownerRepository.save(owner);		
	}

	public void deleteOwner(String id) {
		ownerRepository.delete(id);
	}
	
}
