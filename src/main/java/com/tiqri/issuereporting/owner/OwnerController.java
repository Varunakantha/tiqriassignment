package com.tiqri.issuereporting.owner;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OwnerController {
	
	@Autowired
	private OwnerService ownerServie;

	@RequestMapping("/owners")
	public List<Owner> getAllOwners () {
		return ownerServie.getAllOwners();
	}
	
	@RequestMapping("/owners/{id}")
	public Owner getOwner(@PathVariable String id){
		return ownerServie.getOwner(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/owners")
	public void addOwner (@RequestBody Owner owner){
		ownerServie.addOwner(owner);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/owner/{id}")
	public void updateOwner (@RequestBody Owner owner, @PathVariable String id){
		ownerServie.updateOwner(owner, id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/owner/{id}")
	public void deleteOwner (@PathVariable String id){
		ownerServie.deleteOwner(id);
	}

}
