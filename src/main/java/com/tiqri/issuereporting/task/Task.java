package com.tiqri.issuereporting.task;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.tiqri.issuereporting.owner.Owner;

@Entity
public class Task {
	@Id
	private String id;
	private String description;
	private String timeStamp;
	private String status;
	private String location;
	

	@ManyToOne
	private Owner owner;
	
	public Task(String id, String description, String timeStamp, String ownerId, String status, String location) {
		super();
		this.id = id;
		this.description = description;
		this.timeStamp = timeStamp;
		this.status = status;
		this.location = location;
		this.owner = new Owner (ownerId,"","");
	}
	
	public Task() {
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String gettimeStamp() {
		return timeStamp;
	}
	public void settimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
