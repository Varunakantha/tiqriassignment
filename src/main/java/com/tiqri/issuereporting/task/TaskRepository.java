package com.tiqri.issuereporting.task;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TaskRepository extends CrudRepository<Task,String> {
	
	List<Task> findByOwnerId(String ownerId);

}
