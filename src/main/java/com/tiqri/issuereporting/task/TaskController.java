package com.tiqri.issuereporting.task;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tiqri.issuereporting.owner.Owner;

@RestController
public class TaskController {
	//Test
	@Autowired
	private TaskService taskServie;

	@RequestMapping("/owners/{id}/tasks")
	public List<Task> getAllTasks (@PathVariable String id) {
		return taskServie.getAllTasks(id);
	}
	
	@RequestMapping("/owners/{ownerid}/tasks/{id}")
	public Task getTask(@PathVariable String id){
		return taskServie.getTask(id);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/owners/{ownerid}/tasks")
	public void addTask (@RequestBody Task task,@PathVariable String ownerid){
		task.setOwner(new Owner(ownerid,"",""));
		taskServie.addTask(task);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/owners/{ownerid}/tasks/{id}")
	public void updateTask (@RequestBody Task task,@PathVariable String ownerid, @PathVariable String id){
		task.setOwner(new Owner(ownerid,"",""));
		taskServie.updateTask(task, id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/owners/{ownerid}/tasks/{id}")
	public void deleteTask (@PathVariable String id){
		taskServie.deleteTask(id);
	}

}
