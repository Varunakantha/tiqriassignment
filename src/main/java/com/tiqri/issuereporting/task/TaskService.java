package com.tiqri.issuereporting.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

	@Autowired
	private TaskRepository taskRepository ;
	
	public List<Task> getAllTasks (String topicId) {
		List<Task> tasks = new ArrayList<Task>();
		tasks =(List<Task>) taskRepository.findByOwnerId(topicId);
		return tasks;
	}
	
	public Task getTask (String id){
	return taskRepository.findOne(id);
	}

	public void addTask(Task course) {
		taskRepository.save(course);		
	}

	public void updateTask(Task course, String id) {
		taskRepository.save(course);		
	}

	public void deleteTask(String id) {
		taskRepository.delete(id);
	}
	
}
